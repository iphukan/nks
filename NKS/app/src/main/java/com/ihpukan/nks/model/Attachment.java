package com.ihpukan.nks.model;

import java.util.List;

public class Attachment {
    public String title;
    public String text;
    public String fallback;
    public List<String> markdwn_in;
}
